var module1 = require('./module1');

function action() {
  module1.action();
  console.log("from module2");
}

module.exports = {
  action: action
};
